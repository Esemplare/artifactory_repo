#!/bin/bash
#===============================================================================
#
#          FILE: nginx-install.sh
#
#         USAGE: ./nginx-install.sh
#                (see HELPFUNCTION for parameter usage)
#
#   DESCRIPTION: This script will install and configure Nginx as a reverse
#                proxy for an Artifactory node.
#
#       OPTIONS: ---
#  REQUIREMENTS: Files used by this script are contained in the repo for the
#                script and must be placed in "deploy" directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Steve Esemplare (Tech Consultant), steven.esemplare@invesco.com
#         OWNER: Jesse Feith (System Administrator), jesse.feith@invesco.com
#  ORGANIZATION: ---
#       CREATED: 01/04/2020
#      REVISION: ---
#  MODIFICATION: ---
#         TODOS: ---
#===============================================================================

#-------------------------------------------------------------------------------
# Help function to display usage
#-------------------------------------------------------------------------------
HELPFUNCTION()
{
  echo 'Usage: -p primary';
  echo -e '\t-v  the volume for the Nginx installation';
  echo -e '\t-c  the SSL cert file location - ssl_cert_file';
  echo -e '\t-k  the SSL key file location - ssl_key_file';
  echo -e '\t-s  the server names - server_names';
  echo -e '\t-i  the instance endpoint - instance_endpoint';
  #~(?<repo>.+)\.usappdsoalp102.ops.invesco.net usappdsoalp102.ops.invesco.net artifactory.app.invesco.net ~(?<repo>.+)\.artifactory.app.invesco.net ~(?<repo>.+)\.artifactory.invesco.com artifactory.invesco.com
  #usappdsoalp102.ops.invesco.net
}


#-------------------------------------------------------------------------------
# Process parameters and place into variables
#-------------------------------------------------------------------------------
while getopts "v:" flag; do
  case "$flag" in
    v) volume=$OPTARG;;
    c) ssl_cert_key=$OPTARG;;
    k) ssl_key_file=$OPTARG;;
    s) server_names=$OPTARG;;
    i) instance_endpoint=$OPTARG;;
    ?) HELPFUNCTION && exit 0;;
  esac
done


#-------------------------------------------------------------------------------
# Verify parameters have been submitted
#-------------------------------------------------------------------------------
if [ -z $volume ] || [ -z $ssl_cert_file ] || [ -z $ssl_key_file ] || [ -z $server_names ] || [ -z $instance_endpoint ]; then
  echo ''
  echo -e 'Some or all parameters are missing!';
  echo ''
  HELPFUNCTION;
  exit -1;
fi


#-------------------------------------------------------------------------------
# List parameters for confirmation
#-------------------------------------------------------------------------------
echo ''
echo 'This script will install and configure Nginx as a reverse proxy for an'
echo 'Artifactory as node.'
echo ''
echo 'This script requires that the cert and key file for deployment be placed'
echo 'in the deploy/ssl directory.'
echo ''
echo 'The installation will continue with the following parameters:'
echo ''
echo -e "\tVolume: $volume"
echo ''
echo -e "\tSSL Certificate File: $ssl_cert"
echo -e "\tSSL Key File: $ssl_key"
echo ''
echo -e "\tServer Names: $server_names"
echo ''
echo -e "\tInstance Endpoint: $instance_endpoint"
echo ''
echo 'Make sure you are in the deploy directory of the repository whern running this script.'
echo ''
echo ''


#-------------------------------------------------------------------------------
# Get confirmation to proceed
#-------------------------------------------------------------------------------
while true; do
    read -p "Do you want to continue with the installation and configuration? (y/n) " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes/y or no/n.";;
    esac
done


#-------------------------------------------------------------------------------
# Install nginx
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Installing nginx...'
echo ''
mkdir -p $volume/opt/nginx
yum localinstall --installroot=<$volume/opt/nginx software/nginx-1.18.0-2.el7.ngx.x86_64.rpm
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to install nginx';
  exit;
fi
echo ''
echo 'SUCCESS: nginx installed successfully!'
echo ''


#-------------------------------------------------------------------------------
# Create the artifactory-subdomain.conf file for nginx - this file contains the 
# configuration data for nginx to function as a proxy server for Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Updating nginx artifactory-subdomain.conf file with environment parameters...'
echo ''
cp config_files/artifactory-subdomain.conf.template config_files/artifactory-subdomain.conf
sed -i '' "s/\%\%ssl_cert_file\%\%/$ssl_cert_file/g" config_files/artifactory-subdomain.conf
sed -i '' "s/\%\%ssl_key\%\%/$ssl_key/g" config_files/artifactory-subdomain.conf
sed -i '' "s/\%\%server_names\%\%/$server_names/g" config_files/artifactory-subdomain.conf
sed -i '' "s/\%\%instance_endpoint\%\%/$instance_endpoint/g" config_files/artifactory-subdomain.conf
if [ $? -ne 0 ]; then
  echo '';
  echo 'ERROR: Unable to create system.yaml file';
  exit -1;
fi
echo ''
echo 'SUCCESS: artifactory-subdomain.conf file updated successfully!'
echo ''


#-------------------------------------------------------------------------------
# Copy the updated artifactory-subdomain.conf file to nginx
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated artifactory-subdomain.conf file to nginx...'
echo ''
cp config_files/artifactory-subdomain.conf /$volume/opt/etc/conf.d/artifactory-subdomain.conf
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy artifactory-subdomain.conf file to nginx';
  exit -1;
fi
echo ''
echo 'SUCCESS: artifactory-subdomain.conf file copied successfully!'
echo ''


echo ''
echo '-------------------------------------------------------------------------------'
echo 'Extracting Artifactory to $volume/opt/jfrog/artifactory...'
echo ''