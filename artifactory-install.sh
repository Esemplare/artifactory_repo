#!/bin/bash
#===============================================================================
#
#          FILE: artifactory-install.sh
#
#         USAGE: ./artifactory-install.sh
#                (see HELPFUNCTION for parameter usage)
#
#   DESCRIPTION: This script can be run to install and configure either a 
#                Primary or Secondary node in an Artifactory HA Cluster.
#
#       OPTIONS: ---
#  REQUIREMENTS: Files used by this script are contained in the repo for the
#                script and must be placed in "deploy" directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Steve Esemplare (Tech Consultant), steven.esemplare@invesco.com
#         OWNER: Jesse Feith (System Administrator), jesse.feith@invesco.com
#  ORGANIZATION: ---
#       CREATED: 01/04/2020
#      REVISION: ---
#  MODIFICATION: ---
#         TODOS: ---
#       EXAMPLE: ./artifactory-install.sh -v /Users/steesemp/work -n nodename -p true -h hostname -a artifactorypwd -e endpoint -b bucketname -r region -i identity -c credentials
#===============================================================================

#-------------------------------------------------------------------------------
# Help function to display usage
#-------------------------------------------------------------------------------
HELPFUNCTION()
{
  echo '';
  echo 'Usage: -p primary';
  echo -e '\t-v  the volume for the Artifactory installation';
  echo -e '\t-n  node name that will be used in Artifactory to identify the node';
  echo -e '\t-p  is this the primary node (value of yes or no)';
  echo -e '\t-h  host for the database (provided from the deployment)';
  echo -e '\t-a  password for artifactory to use to connect to database';
  echo -e '\t-e  the s3 endpoint';
  echo -e '\t-b  the s3 bucket name';
  echo -e '\t-r  the s3 region';
  echo -e '\t-i  the identity to use for accessing the s3 bucket';
  echo -e '\t-c  the initial credentials for the identity (will refresh automatically)';
  echo '';
}


#-------------------------------------------------------------------------------
# Process parameters and place into variables
#-------------------------------------------------------------------------------
while getopts "v:n:p:h:a:e:b:r:i:c:" flag; do
  case "$flag" in
    v) volume=$OPTARG;;
    n) node_name=$OPTARG;;
    p) primary_node=$OPTARG;;
    h) db_host_name=$OPTARG;;
    a) af_db_password=$OPTARG;;
    e) s3_endpoint=$OPTARG;;
    b) s3_bucket_name=$OPTARG;;
    r) s3_region=$OPTARG;;
    i) s3_identity=$OPTARG;;
    c) s3_identity_credentials=$OPTARG;;
    ?) HELPFUNCTION && exit 0;;
  esac
done


#-------------------------------------------------------------------------------
# Verify parameters have been submitted
#-------------------------------------------------------------------------------
if [ -z $volume ] || [ -z $primary_node ] || [ -z $node_name ] || [ -z $db_host_name ] || [ -z $af_db_password ] || [ -z $s3_endpoint ] || [ -z $s3_bucket_name ] || [ -z $s3_region ] || [ -z $s3_identity ] || [ -z $s3_identity_credentials ]; then
  echo ''
  echo -e 'Some or all parameters are missing!';
  echo ''
  HELPFUNCTION;
  exit -1;
fi


#-------------------------------------------------------------------------------
# List parameters for confirmation
#-------------------------------------------------------------------------------
echo ''
echo 'This script will install JFrog Artifactory as either the primary node or'
echo 'a secondary node in an Artifactory HA cluster configuration.'
echo ''
echo 'This script requires that the database already be setup and configured.'
echo ''
echo 'The installation will continue with the following parameters:'
echo ''
echo -e "\tVolume: $volume"
echo ''
echo -e "\tNode Name: $node_name"
echo -e "\tPrimary Node: $primary_node"
echo ''
echo -e "\tDB Host Name: $db_host_name"
echo -e "\tArtifactory DB Password: $af_db_password"
echo ''
echo -e "\tS3 Endpoint: $s3_endpoint"
echo -e "\tS3 Bucket Name: $s3_bucket_name"
echo -e "\tS3 Region: $s3_region"
echo -e "\tS3 Identity: $s3_identity"
echo -e "\tS3 Identity Credentials: $s3_identity_credentials"
echo ''
echo 'Make sure you are in the deploy directory of the repository whern running this script.'
echo ''
echo ''


#-------------------------------------------------------------------------------
# Get confirmation to proceed
#-------------------------------------------------------------------------------
while true; do
    read -p "Do you want to continue with the installation and configuration? (y/n) " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes/y or no/n.";;
    esac
done


#-------------------------------------------------------------------------------
# Extract and copy the artifactory code to /opt/jfrog/artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Extracting Artifactory...'
echo ''
tar -xvf software/jfrog-artifactory-pro-7.10.2-linux.tar.gz
mkdir -p $volume/opt/jfrog/artifactory
mv artifactory-pro-7.10.2/* $volume/opt/jfrog/artifactory/
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to install postgresql12 client';
  exit;
fi
echo ''
echo 'SUCCESS: Artifactory extracted!'
echo ''


#-------------------------------------------------------------------------------
# Install the postgresl client
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Installing Postgresql client...'
echo ''
mkdir -p $volume/opt/postgresql
yum localinstall --installroot=<$volume/opt/postgresql software/postgresql12
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to install postgresql12 client';
  exit;
fi
echo ''
echo 'SUCCESS: Postgresql client installed successfully!'
echo ''


#-------------------------------------------------------------------------------
# Install the postgresl jdbc driver
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Installing Postgresql JDBC driver...'
echo ''
cp software/postgresql-42.2.18.jar $volume/opt/jfrog/var/bootstrap/artifactory/tomcat/lib/postgresql-42.2.18.jar
chmod +r $volume/opt/jfrog/var/bootstrap/artifactory/tomcat/lib/postgresql-42.2.18.jar
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to install pistgresql jdbc driver';
  exit;
fi
echo ''
echo 'SUCCESS: Postgresql JDBC driver setup successfully!'
echo ''


#-------------------------------------------------------------------------------
# Create the system.yaml file for Artifactory - this file contains the 
# configuration data for Artifactory and it's related services
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Updating Artifactory system.yaml file with environment parameters...'
echo ''
cp config_files/system.yaml.template config_files/system.yaml
sed -i '' "s/\%\%node_name\%\%/$node_name/g" config_files/system.yaml
sed -i '' "s/\%\%primary_node\%\%/$primary_node/g" config_files/system.yaml
sed -i '' "s/\%\%db_host_name\%\%/$db_host_name/g" config_files/system.yaml
sed -i '' "s/\%\%af_db_password\%\%/$af_db_password/g" config_files/system.yaml
if [ $? -ne 0 ]; then
  echo '';
  echo 'ERROR: Unable to create system.yaml file';
  exit -1;
fi
echo ''
echo 'SUCCESS: system.yaml file updated successfully!'
echo ''



#-------------------------------------------------------------------------------
# Copy the updated system.yaml file to Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated system.yaml file to Artifactory...'
echo ''
cp config_files/system.yaml /$volume/opt/jfrog/artifactory/var/etc/system.yaml
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy system.yaml file to Artifactory';
  exit -1;
fi
echo ''
echo 'SUCCESS: system.yaml file copied successfully!'
echo ''



#-------------------------------------------------------------------------------
# Create the binarystore.xml file for Artifactory - this file contains the 
# configuration data for Artifactory to connect to and store binaries in S3
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Updating Artifactory binarystore.yaml file with environment parameters...'
echo ''
cp $config_files/binarystore.xml.template $volume/deploy/config_files/binarystore.xml
sed -i '' "s/\%\%s3_endpoint\%\%/$s3_endpoint/g" $volume/deploy/config_files/binarystore.xml
sed -i '' "s/\%\%s3_bucket_name\%\%/$s3_bucket_name/g" $volume/deploy/config_files/binarystore.xml
sed -i '' "s/\%\%s3_region\%\%/$s3_region/g" $volume/deploy/config_files/binarystore.xml
sed -i '' "s/\%\%s3_identity\%\%/$s3_identity/g" $volume/deploy/config_files/binarystore.xml
sed -i '' "s/\%\%s3_identity_credentials\%\%/$s3_identity_credentials/g" $volume/deploy/config_files/binarystore.xml
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to create binarystore.xml file';
  exit -1;
fi
echo ''
echo 'SUCCESS: binarystore.xml file updated successfully!'
echo ''


#-------------------------------------------------------------------------------
# Copy the updated binarystore.xml file to Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated binarystore.xml file to Artifactory...'
echo ''
cp config_files/binarystore.xml /$volume/opt/jfrog/artifactory/var/etc/artifactory/binarystore.xml
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy binarystore.xml file to Artifactory';
  exit -1;
fi
echo ''
echo 'SUCCESS: binarystore.xml file copied successfully!'
echo ''



# create logback.xml file
#-------------------------------------------------------------------------------
# Copy the updated logback.xml file to Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated logback.xml file to Artifactory...'
echo ''
cp config_files/logback.xml.template config_files/logback.xml
cp config_files/logback.xml /$volume/opt/jfrog/artifactory/var/etc/artifactory/logback.xml
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy logback.xml file to Artifactory';
  exit -1;
fi
echo ''
echo 'SUCCESS: logback.xml file copied successfully!'
echo ''


echo ''
echo '-------------------------------------------------------------------------------'
echo 'SUCCESS: Script completed!'
echo ''