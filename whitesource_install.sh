#!/bin/bash
#===============================================================================
#
#          FILE: whitesource_install.sh
#
#         USAGE: ./whitesource_install.sh
#                (see HELPFUNCTION for parameter usage)
#
#   DESCRIPTION: This script will install and configure Whitesource plugin for
#                an Artifactory node. In addition, it will also configure a cron
#                job to execute a Whitesource scan.
#
#       OPTIONS: ---
#  REQUIREMENTS: Files used by this script are contained in the repo for the
#                script and must be placed in "deploy" directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Steve Esemplare (Tech Consultant), steven.esemplare@invesco.com
#         OWNER: Jesse Feith (System Administrator), jesse.feith@invesco.com
#  ORGANIZATION: ---
#       CREATED: 01/04/2020
#      REVISION: ---
#  MODIFICATION: ---
#         TODOS: ---
#===============================================================================

#-------------------------------------------------------------------------------
# Help function to display usage
#-------------------------------------------------------------------------------
HELPFUNCTION()
{
  echo 'Usage: -p primary';
  echo -e '\t-v  the volume for the Whitesource installation';
  echo -e '\t-a  the whitesource api key';
  echo -e '\t-n  the artifactory node name';
}


#-------------------------------------------------------------------------------
# Process parameters and place into variables
#-------------------------------------------------------------------------------
while getopts "v:a:n:" flag; do
  case "$flag" in
    v) volume=$OPTARG;;
    a) api_key=$OPTARG;;
    n) node_name=$OPTARG;;
    ?) HELPFUNCTION && exit 0;;
  esac
done


#-------------------------------------------------------------------------------
# Verify parameters have been submitted
#-------------------------------------------------------------------------------
if [ -z $volume ] || [ -z $api_key ] || [ -z $node_name ] ; then
  echo ''
  echo -e 'Some or all parameters are missing!';
  echo ''
  HELPFUNCTION;
  exit -1;
fi


#-------------------------------------------------------------------------------
# List parameters for confirmation
#-------------------------------------------------------------------------------
echo ''
echo 'This script will install and configure the Whitesource plugin for an'
echo 'Artifactory node.'
echo ''
echo 'The installation will continue with the following parameters:'
echo ''
echo -e "\tVolume: $volume"
echo ''
echo -e "\tAPI Key: $api_key"
echo ''
echo -e "\tNode Name: $node_name"
echo ''


#-------------------------------------------------------------------------------
# Get confirmation to proceed
#-------------------------------------------------------------------------------
while true; do
    read -p "Do you want to continue with the installation and configuration? (y/n) " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes/y or no/n.";;
    esac
done


#-------------------------------------------------------------------------------
# Setup the Artifactory plugin directories
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Creating Artifactoruy plugin directory '
echo ''
mkdir -p $volume/opt/jfrog/artifactory/var/etc/artifactory/plugins/lib
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to create plugin directory';
  exit;
fi
echo ''
echo 'SUCCESS: Artifactory plugin directory created!'
echo ''


#-------------------------------------------------------------------------------
# Extract and install the whitesource plugin
#-------------------------------------------------------------------------------
whitesource-artifactory-plugin-20.9.1.zip
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Extracting Whitesource...'
echo ''
tar -xvf software/whitesource-artifactory-plugin-20.9.1.zip
cp software/whitesource-artifactory-plugin-20.9.1/*.jar $volume/opt/jfrog/artifactory/var/etc/artifactory/plugins/lib/
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to extract and copy the whitesource plugin';
  exit;
fi
echo ''
echo 'SUCCESS: White source plugin extrated and copied!'
echo ''


#-------------------------------------------------------------------------------
# Create the whitesource-artifactory-plugin.properties file for Artifactory - 
# this file contains the configuration data for the whitesource plugin
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Updating Artifactory whitesource-artifactory-plugin.properties file with environment parameters...'
echo ''
cp config_files/whitesource-artifactory-plugin.properties.template config_files/whitesource-artifactory-plugin.properties
sed -i '' "s/\%\%node_name\%\%/$node_name/g" config_files/whitesource-artifactory-plugin.properties
sed -i '' "s/\%\%api_key\%\%/$api_key/g" config_files/whitesource-artifactory-plugin.properties
if [ $? -ne 0 ]; then
  echo '';
  echo 'ERROR: Unable to create whitesource-artifactory-plugin.properties file';
  exit -1;
fi
echo ''
echo 'SUCCESS: whitesource-artifactory-plugin.properties file updated successfully!'
echo ''


#-------------------------------------------------------------------------------
# Copy the updated whitesource-artifactory-plugin.properties file to Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated whitesource-artifactory-plugin.properties file to Artifactory...'
echo ''
cp config_files/whitesource-artifactory-plugin.properties /$volume/opt/jfrog/artifactory/var/etc/plugins/whitesource-artifactory-plugin.properties
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy whitesource-artifactory-plugin.properties file to Artifactory';
  exit -1;
fi
echo ''
echo 'SUCCESS: whitesource-artifactory-plugin.properties file copied successfully!'
echo ''


#-------------------------------------------------------------------------------
# Copy the updated whitesource-artifactory-plugin.groovy file to Artifactory
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Copying updated whitesource-artifactory-plugin.groovy file to Artifactory...'
echo ''
cp config_files/whitesource-artifactory-plugin.groovy /$volume/opt/jfrog/artifactory/var/etc/plugins/whitesource-artifactory-plugin.groovy
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to copy whitesource-artifactory-plugin.groovy file to Artifactory';
  exit -1;
fi
echo ''
echo 'SUCCESS: whitesource-artifactory-plugin.groovy file copied successfully!'
echo ''


#-------------------------------------------------------------------------------
# Notify user that Artifactory needs to be restarted
#-------------------------------------------------------------------------------
echo ''
echo ''
echo 'NEXT STEPS: Artifactory must be restarted for these changes to take effect!'
echo ''
