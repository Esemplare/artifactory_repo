#!/bin/bash
#===============================================================================
#
#          FILE: artifactory_database_setup.sh
#
#         USAGE: ./artifactory_database_setup.sh
#                (see HELPFUNCTION for parameter usage)
#
#   DESCRIPTION: This script should ONLY be run for a NEW environment as it will 
#                connect to the RDS host, create the artifactory database, add 
#                the required database user and configure the required role.
#                This script can be run on any EC2 node in the Artifactory HA 
#                cluster.
#
#       OPTIONS: ---
#  REQUIREMENTS: Files used by this script are contained in the repo for the
#                script and must be placed in "deploy" directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Steve Esemplare (Tech Consultant), steven.esemplare@invesco.com
#         OWNER: Jesse Feith (System Administrator), jesse.feith@invesco.com
#  ORGANIZATION: ---
#       CREATED: 01/04/2020
#      REVISION: ---
#  MODIFICATION: ---
#         TODOS: ---
#       EXAMPLE: ./db-setup.sh -v /Users/steesemp/work -h hostname -m masteruser -p masterpwd -a artifactorypwd
#===============================================================================

#-------------------------------------------------------------------------------
# Help function to display usage
#-------------------------------------------------------------------------------
HELPFUNCTION()
{
  echo '';
  echo 'Usage: -h db_host_name -m db_master_user -p db_master_password -a db_password';
  echo -e '\t-v  the volume for the Artifactory installation';
  echo -e '\t-h  host for the database (provided from the deployment)';
  echo -e '\t-m  name of the root user (provided from the deployment - typically muser)';
  echo -e '\t-p  password for the database (provided from the deployment)';
  echo -e '\t-a  password for artifactory to use to connect to database';
  echo ''
}


#-------------------------------------------------------------------------------
# Process parameters and place into variables
#-------------------------------------------------------------------------------
while getopts "v:h:m:p:a:" flag; do
  case "$flag" in
    v) volume=$OPTARG;;
    h) db_host_name=$OPTARG;;
    m) db_master_user=$OPTARG;;
    p) db_master_password=$OPTARG;;
    a) af_db_password=$OPTARG;;
    ?) HELPFUNCTION && exit 0;;
  esac
done


#-------------------------------------------------------------------------------
# Verify parameters have been submitted
#-------------------------------------------------------------------------------
if [ -z $volume ] || [ -z $db_host_name ] || [ -z $db_master_user ] || [ -z $db_master_password ] || [ -z $af_db_password ] ; then
  echo '';
  echo -e 'Some or all parameters are missing!';
  echo '';
  HELPFUNCTION;
  exit -1;
fi


#-------------------------------------------------------------------------------
# List parameters for confirmation
#-------------------------------------------------------------------------------
echo ''
echo 'This script should ONLY be run for a NEW environment as it will connect to'
echo 'the RDS host, create the artifactory database, add the required database user'
echo 'and configure the required role. This script can be run on any EC3 node in the'
echo 'Artifactory HA cluster.'
echo ''
echo 'The database setup will continue with the following parameters:'
echo ''
echo -e "\tVolume: $volume"
echo ''
echo -e "\tDB Host Name: $db_host_name"
echo ''
echo -e "\tDB Master User: $db_master_user"
echo -e "\tDB Master Password: $db_master_password"
echo ''
echo -e "\tArtifactory DB Password: $af_db_password"
echo ''
echo 'Make sure you are in the deploy directory of the repository whern running this script.'
echo ''
echo ''


#-------------------------------------------------------------------------------
# Get confirmation to proceed
#-------------------------------------------------------------------------------
while true; do
    read -p "Do you want to continue with the installation and setup? (y/n) " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes/y or no/n.";;
    esac
done


#-------------------------------------------------------------------------------
# Install the postgresql client
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Installing postgresl client...'
echo ''
cd deploy/software
mkdir -p $volume/opt/postgresql
yum localinstall --installroot=<$volume/opt/postgresql postgresql12
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to install postgresql12 client';
  exit;
fi
echo ''
echo 'SUCCESS: Postgresql client installed successfully!'
echo ''


#-------------------------------------------------------------------------------
# Create SQL command file to be executed - this will create the artifactory
# user, create the artifactory database, and grant priviliges to the user
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Updating Postgresql SQL setup file with environment parameters...'
echo ''
cd ../../deploy/config_files
cp artifactory_postgresql_setup.sql.template artifactory_postgresql_setup.sql
sed -i '' "s/\%\%af_db_password\%\%/$af_db_password/g" artifactory_postgresql_setup.sql
if [ $? -ne 0 ]; then
  echo ''
  echo 'ERROR: Unable to create sql file artifactory_postgresql_setup.sql';
  exit -1;
fi
echo ''
echo 'SUCCESS: Postgresql SQL file updated with environment parameters!'
echo ''

# set postgreSQL password variable for use by psql client
set PGPASSWORD=$db_master_password

# connect to database using postgresql client and execute the sql commands
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Connectin to Postgresql and executing SQL setup file...'
echo ''
psql --host=$db_host_name --port=5432 --username=$db_master_user --password dbname=postgres -a -f artifactory_postgresql_setup.sql
if [ $? -ne 0 ]; then
  echo ''
  echo 'Unable to connect to psql and execute commands in artifactory_postgresql_setup.sql';
  exit -1;
fi
echo ''
echo 'SUCCESS: Postgresql SQL file executed successfully!'
echo ''


#-------------------------------------------------------------------------------
# Create SQL command file to be executed - this will verify that a connection to
# the artifactory database is successful.
#-------------------------------------------------------------------------------
echo ''
echo '-------------------------------------------------------------------------------'
echo 'Verifying connection to Postgresql...'
echo ''

cp artifactory_postgresql_verify.sql.template artifactory_postgresql_verify.sql

# set postgreSQL password variable for use by psql client for verification
set PGPASSWORD=$af_db_password

# check database connection is working as expected
psql --host=$db_host_name --port=5432 --username=artifactory --password dbname=artifactory -a -f artifactory_postgresql_verify.sql
if [ $? ne 0]; then
  echo ''
  echo 'ERROR: Unable to verify postgresql is setup/configured correctly.';
  exit;
fi
echo 'SUCCESS: Postgresql SQL verified!'
echo ''


echo ''
echo '-------------------------------------------------------------------------------'
echo 'SUCCESS: Script completed!'
echo ''