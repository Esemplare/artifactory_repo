# artifactory deployment scripts

    * What is the 'artifactory-deployment-scripts'
    * Folder structure
    * Source Repostiory

    
## What is the 'artifactory-deployment-scripts'
The scripts to deploy the various components of artifactory
-db-setup.sh - Required ONLY for a new deployment of an environment
-artifactory-install.sh - Can be used to install a primary or secondary node
-nginx-install.sh - Can be used to install the reverse proxy for an artifactory node
-whitesource-install.sh - Can be used to install the whitesource plugin on the primary node ONLY


## Folder structure
The folder structure will be
  -deploy/config_files (template configuration files for artifactory, nginx, and whitesource)
  -deploy/software (software currently deployed - postgresql client, postgresql jdbc driver, artifactory, nginx, whitesource)


## Source Rrespository
https://[SOURCE REPO]

