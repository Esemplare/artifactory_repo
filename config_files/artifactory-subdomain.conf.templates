## add ssl entries when https has been set in config
ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
ssl_certificate      /var/opt/jfrog/artifactory/etc/ssl/artifactory.invesco.com.crt;
ssl_certificate_key  /var/opt/jfrog/artifactory/etc/ssl/artifactory.invesco.com.key;
ssl_session_cache shared:SSL:1m;
ssl_prefer_server_ciphers   on;

## server configuration
server {
    listen 443 ssl;

    ##server_name usappdsoalp102.ops.invesco.net;
    server_name ~(?<repo>.+)\.usappdsoalp102.ops.invesco.net usappdsoalp102.ops.invesco.net artifactory.app.invesco.net ~(?<repo>.+)\.artifactory.app.invesco.net ~(?<repo>.+)\.artifactory.invesco.com artifactory.invesco.com;

    if ($http_x_forwarded_proto = '') {
        set $http_x_forwarded_proto  $scheme;
    }

    ## Application specific logs
    #access_log /var/log/nginx/usappdsoalp102.ops.invesco.net-access.log timing;
    #error_log /var/log/nginx/usappdsoalp102.ops.invesco.net-error.log;

    # redirect
    rewrite ^/$ /ui/ redirect;
    rewrite ^/ui$ /ui/ redirect;
    rewrite ^/(v1|v2)/(.*) /artifactory/api/docker/$repo/$1/$2;
    #rewrite ^/(v1|v2)/(.*) /api/docker/$repo/$1/$2;

    chunked_transfer_encoding on;
    client_max_body_size 0;

    location / {
      proxy_connect_timeout  7200;
      proxy_read_timeout     7200;
      proxy_send_timeout     7200;
      send_timeout           7200;

      proxy_http_version        1.1; 
      proxy_request_buffering   off;
      proxy_buffering           off;

      proxy_pass_header   Server;
      proxy_cookie_path   ~*^/.* /;
      proxy_buffer_size 128k;
      proxy_buffers 40 128k;
      proxy_busy_buffers_size 128k;
      proxy_pass          http://usappdsoalp102.ops.invesco.net:8082;
      proxy_set_header    X-JFrog-Override-Base-Url $http_x_forwarded_proto://$host:$server_port;
      proxy_set_header    X-Forwarded-Port  $server_port;
      proxy_set_header    X-Forwarded-Proto $http_x_forwarded_proto;
      proxy_set_header    Host              $http_host;
      proxy_set_header    X-Forwarded-For   $proxy_add_x_forwarded_for;

      location ~ ^/artifactory/ {
        proxy_pass    http://usappdsoalp102.ops.invesco.net:8081;
      }
    }
}
