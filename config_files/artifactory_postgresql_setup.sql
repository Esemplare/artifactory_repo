CREATE USER artifactory WITH PASSWORD 'a';
ALTER ROLE artifactory CREATEDB;
CREATE DATABASE artifactory WITH OWNER=artifactory ENCODING='UTF8';
GRANT ALL PRIVILEGES ON DATABASE artifactory TO artifactory;
